
// 表单校验规则由 schema2code 生成，不建议直接修改校验规则，而建议通过 schema2code 生成, 详情: https://uniapp.dcloud.net.cn/uniCloud/schema



const validator = {
  "name": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "string"
      },
      {
        "minLength": 2
      }
    ],
    "label": "姓名"
  },
  "mobile": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "string"
      },
      {
        "pattern": "^\\+?[0-9-]{3,20}$"
      }
    ],
    "label": "手机号码"
  },
  "idcard_type": {
    "rules": [
      {
        "format": "int"
      },
      {
        "range": [
          {
            "text": "身份证",
            "value": 0
          },
          {
            "text": "护照",
            "value": 1
          },
          {
            "text": "军官证",
            "value": 2
          },
          {
            "text": "港澳身份证",
            "value": 3
          }
        ]
      }
    ],
    "defaultValue": 0,
    "label": "证件类型"
  },
  "idcard": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "证件号码"
  },
  "address": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "string"
      },
      {
        "minLength": 3
      }
    ],
    "label": "本地住址"
  },
  "from_abroad": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "bool"
      }
    ],
    "defaultValue": false,
    "label": "是否来自国外"
  },
  "area_code": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "来源地省市区"
  },
  "from_address": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "string"
      },
      {
        "minLength": 3
      }
    ],
    "label": "来源地地址"
  },
  "transport": {
    "rules": [
      {
        "format": "int"
      },
      {
        "range": [
          {
            "text": "飞机",
            "value": 0
          },
          {
            "text": "火车",
            "value": 1
          },
          {
            "text": "长途汽车",
            "value": 2
          },
          {
            "text": "自驾",
            "value": 3
          },
          {
            "text": "轮船",
            "value": 4
          }
        ]
      }
    ],
    "label": "交通工具类型"
  },
  "transport_no": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "交通工具班次"
  },
  "status": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "int"
      },
      {
        "range": [
          {
            "text": "居家隔离",
            "value": 0
          },
          {
            "text": "集中隔离",
            "value": 1
          },
          {
            "text": "解除隔离",
            "value": 2
          },
          {
            "text": "送医",
            "value": 3
          },
          {
            "text": "出院居家隔离",
            "value": 4
          },
          {
            "text": "康复",
            "value": 5
          },
          {
            "text": "死亡",
            "value": 6
          }
        ]
      }
    ],
    "label": "状态"
  },
  "comment": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "备注"
  }
}

const enumConverter = {
  "idcard_type_valuetotext": {
    "0": "身份证",
    "1": "护照",
    "2": "军官证",
    "3": "港澳身份证"
  },
  "transport_valuetotext": {
    "0": "飞机",
    "1": "火车",
    "2": "长途汽车",
    "3": "自驾",
    "4": "轮船"
  },
  "status_valuetotext": {
    "0": "居家隔离",
    "1": "集中隔离",
    "2": "解除隔离",
    "3": "送医",
    "4": "出院居家隔离",
    "5": "康复",
    "6": "死亡"
  }
}

export { validator, enumConverter }
