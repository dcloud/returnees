# 返乡人员管理系统

本系统给公务人员使用，管理返乡人员数据资料。

返乡人员自助登记另见项目：[返乡人员自助登记](https://gitee.com/dcloud/returnees_selfreport)

基于uniCloud admin，使用scheme2code生成管理页面。

1. 导入本项目到HBuilderX后
2. 修改manifest里的appid
3. 自己新建一个uniCloud服务空间，在项目的uniCloud目录上绑定好服务空间

